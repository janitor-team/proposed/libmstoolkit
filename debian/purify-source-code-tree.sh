#!/bin/sh 

set -e

if ! test $(basename $(pwd)) = "tarballs"
    then
    echo "Not in the tarballs directory."
    exit 1
fi

PROJ_NAME=libmstoolkit

rm -rf ${PROJ_NAME}

result=$(svn checkout http://mstoolkit.googlecode.com/svn/trunk/ ${PROJ_NAME} | grep "Checked out revision [0-9]\+\.")

echo ${result}

svnRevision=$(echo ${result} | awk '{print $4}'| sed 's/\.//g')

version="${svnRevision}~svn"
mv ${PROJ_NAME} ${PROJ_NAME}-${version}

cd ${PROJ_NAME}-${version}

# Remove the .svn directory and other unused stuff
rm -rvf .svn
rm -vf *.vcproj
rm -rvf src/expat-* src/sqlite-* src/zLib-*

cd ..

tar cvzf ${PROJ_NAME}-${version}.tar.gz ${PROJ_NAME}-${version}

echo "Produced ${PROJ_NAME}-${version}.tar.gz"

